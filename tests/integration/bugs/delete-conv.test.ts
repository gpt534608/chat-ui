import { exec } from "child_process";
import { MongoMemoryServer } from "mongodb-memory-server";
import puppeteer from "puppeteer";
import { setDefaultOptions } from "expect-puppeteer";
import { mockPrompts } from "../../fixtures/prompts";
import selectors from "../../fixtures/selectors";
import { createUser } from "../../support/utils";
import type { Browser, Page } from "puppeteer";

// NOTE:
// For mongodb-memory-server
// When you are not using a global instance for tests and run multi-threaded tests, it can cause Race Conditions.

describe("Deleting conversations does not freeze app", () => {
	// let con: MongoClient;
	let mongoServer: MongoMemoryServer;
	let browser: Browser;
	let page: Page;
	const calcTime = mockPrompts.length * 5000 + 15000;
	let isFirstPromptIgnored = false;
	let adjustedPromptsCount = mockPrompts.length + (isFirstPromptIgnored ? 1 : 0);

	// TODO: use env
	const homePageUrl = "http://localhost:5173";

	setDefaultOptions({ timeout: 2000 });

	beforeAll(async () => {
		// start mongo memory server and get uri
		// TODO: make MongoMemoryServer a global instance
		// TODO: first time it runs, it download mongodb, so adjust time accordingly
		// mongoServer = await MongoMemoryServer.create({ binary: { version: "7.0.2" }, instance: {} });
		mongoServer = await MongoMemoryServer.create({ instance: {} });
		// console.log(`mongoServer: `, mongoServer.getUri());
		const mongoUri = mongoServer.getUri();
		const userAndPass = "admin";
		await createUser(mongoUri, userAndPass);

		// update env var to use mongo memory server
		// and launch chat-ui app
		const cleanedMongoUri = mongoUri.split(/mongodb:\/\/|\//)[1];
		const updatedEnv = {
			...process.env,
			MONGODB_URL: cleanedMongoUri,
			GPT_MONGODB_ID: userAndPass,
			GPT_MONGODB_PASSWORD: userAndPass,
		};
		exec("npm run dev", { env: updatedEnv }, (error, stdout, stderr) => {
			if (error) {
				console.error(`Error: ${error.message}`);
				return;
			}
			if (stderr) {
				console.error(`stderr: ${stderr}`);
				return;
			}
			console.log(`stdout: ${stdout}`);
		});

		// launch puppeteer
		browser = await puppeteer.launch({
			headless: false,
			slowMo: 80,
			// devtools: true,
			args: ["--window-size=1080,720"],
		});
		page = await browser.newPage();
		await page.goto(homePageUrl);
	}, 10 * 60 * 1000);

	afterAll(async () => {
		// if (con) await con.close();
		// if (mongoServer) await mongoServer.stop();
		// await browser.close();
	});

	test("Clicks Btn start chatting if it exists", async () => {
		// TODO: save this in /utils
		const findButtonByText = async (text: string) => {
			const btns = Array.from(document.querySelectorAll("button"));
			return btns.find((btn) => btn.textContent?.includes(text));
		};

		const btnStartChatting = await page.evaluate(findButtonByText, "Start chatting");

		if (btnStartChatting) {
			await expect(page).toClick("button", { text: "Start chatting" });
		} else {
			await expect(page).toMatchElement("span", { text: "ChatUI" });
		}
	});

	test(
		`adds ${mockPrompts.length} conversations`,
		async () => {
			// jestExpect.setTimeout(10000);
			const {
				imgHomeLogoSelector,
				inputSelector,
				btnSubmitSelector,
				responseSelector,
				btnConvLinks,
			} = selectors;

			for (let i = 0; i < mockPrompts.length; i++) {
				// console.log('### mockPrompts[i].message: ', mockPrompts[i].message);

				// find input and ask prompt
				await page.type(inputSelector, mockPrompts[i].message);
				await page.click(btnSubmitSelector);

				// bug workaround - bug `first prompt ignored`
				// if imgHomeLogoSelector exists, repeat the 1st prompt
				// && (await page.$(imgHomeLogoSelector))
				if (i === 0) {
					console.log("### imgHomeLogoSelector exists, repeating 1st prompt");
					await new Promise((res) => setTimeout(res, 3000));
					// && (await page.$(imgHomeLogoSelector))
					if (await page.$(imgHomeLogoSelector)) {
						isFirstPromptIgnored = true;
						adjustedPromptsCount = mockPrompts.length + (isFirstPromptIgnored ? 1 : 0);
						await page.goto(homePageUrl);
						// await page.click("a[href='/']");
						await new Promise((res) => setTimeout(res, 1000));
						await page.type(inputSelector, mockPrompts[i].message);
						await page.click(btnSubmitSelector);
					}
				}

				// wait for response
				await page.waitForSelector(responseSelector);
				const responseText = await page.$eval(responseSelector, (el) => el.textContent);
				// console.log(`responseText: `, responseText);

				await page.goto(homePageUrl);
			}

			const totalConvLength = await page.$$eval(btnConvLinks, (el) => el.length);

			// debugger;
			// console.log(`totalConvLength: `, totalConvLength);
			expect(totalConvLength).toEqual(adjustedPromptsCount);
			// console.log(`${mockPrompts.length} conversations added`);

			// done();
		},
		calcTime
	);

	test(
		`deletes ${mockPrompts.length} conversations`,
		async () => {
			const { btnConvLinks, btnDeleteRequest, btnDeleteConfirm } = selectors;

			// const totalConvLength = await page.$$eval(btnConvLinks, el => el.length);
			// div a:nth-child(2) button[title="Delete conversation"]
			// await page.click(`${btnConvLinks}:nth-child(2) ${btnDeleteRequest}`);

			// first conversation link
			await page.hover(`${btnConvLinks}:nth-child(2)`);
			await page.hover(`${btnConvLinks}:nth-child(2) ${btnDeleteRequest}`);
			// await page.click('div a:nth-child(2) button[title="Delete conversation"]');
			await page.click(`${btnConvLinks}:nth-child(2) ${btnDeleteRequest}`);

			for (let i = 0; i < adjustedPromptsCount; i++) {
				await page.click(`${btnConvLinks}:nth-child(2) ${btnDeleteConfirm}`);

				// expect(totalConvLength).toEqual(mockPrompts.length -i-1);
			}

			const totalConvLength = await page.$$eval(btnConvLinks, (el) => el.length);
			expect(totalConvLength).toEqual(0);
			// console.log('50 conversations deleted');
		},
		calcTime / 2
	);

	test("app is not frozen", async () => {
		// TODO: DRY this

		const { inputSelector, btnSubmitSelector, responseSelector, btnConvLinks } = selectors;

		await page.type(inputSelector, mockPrompts[0].message);

		await page.click(btnSubmitSelector);

		await page.waitForSelector(responseSelector);

		const responseText = await page.$eval(responseSelector, (el) => el.textContent);
		// console.log(`responseText: `, responseText);

		const totalConvLength = await page.$$eval(btnConvLinks, (el) => el.length);

		expect(totalConvLength).toEqual(1);

		// console.log('App is not frozen');
	}, 10000);
});
