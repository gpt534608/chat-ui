import { MongoClient } from "mongodb";

export async function createUser(mongoUri: string, userAndPass: string) {
	const client = new MongoClient(mongoUri);
	await client.connect();

	const adminDb = client.db("admin");
	await adminDb.command({
		createUser: userAndPass,
		pwd: userAndPass,
		roles: [{ role: "root", db: "admin" }],
	});

	await client.close();
}
