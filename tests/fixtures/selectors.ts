const selectors = {
	imgHomeLogoSelector: "div#app #slogan span.splash",
	inputSelector: 'div#app form textarea[enterkeyhint="send"]',
	btnSubmitSelector: 'div#app form button[type="submit"]',
	responseSelector: 'div#app div[role="presentation"] div.prose > *',

	btnConvLinks: "div#app > div > nav:nth-child(3) > div:nth-child(2) a",

	btnDeleteRequest: 'button[title="Delete conversation"]',
	btnDeleteConfirm: 'button[title="Confirm delete action"]',
};

export default selectors;
