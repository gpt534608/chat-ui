/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
	preset: "ts-jest",
	testEnvironment: "node",
	testMatch: ["**/tests/integration/**/*.test.ts"],
	transform: { "^.+\\.ts$": "ts-jest" },
};
