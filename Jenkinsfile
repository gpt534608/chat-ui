#!/usr/bin/env groovy
@Library(['hcl-appscan-integration', 'cplib@release/2.0.0']) _
import com.bcbsfl.paas.cplib.helpers.Workspace
import com.bcbsfl.paas.cplib.utils.OpenShift
import com.bcbsfl.paas.cplib.helpers.Build
import com.bcbsfl.paas.cplib.utils.Git

final wspace = new Workspace()
final ose = new OpenShift()
final build = new Build()
final ose4 = "/ocp4/bin/oc"
final NODEJS_TOOL_VERSION= "NodeJS-18.12.1"
def git= new Git()

def GIT_URL = 'git@edclgitp401.bcbsfl.com:git-gpt/chat-ui.git'

def APP_NAME = null
def APP_VERSION = null
def APP_RELEASE_TAG = null
def mergeComments = ''
def GIT_COMMIT = null
def branchName = ""

pipeline {
  agent { label 'nodejs-vm && rhel8' }
  tools {nodejs "${NODEJS_TOOL_VERSION}"}

  triggers {
    gitlab(
      triggerOnPush: true,
      triggerOnMergeRequest: true,
      branchFilterType: 'RegexBasedFilter',
      targetBranchRegex: '(.*release.*|.*main.*)'
    )
  }
  environment {
    MAIL_TO="sai.doppalapudi@bcbsfl.com"
  }
  options {
    disableConcurrentBuilds()
    buildDiscarder(logRotator(numToKeepStr: '30', daysToKeepStr: '100'))
    gitlabConnection('Gitlab_prod')
    gitlabCommitStatus(name: 'jenkins')
    skipDefaultCheckout(true)
    timestamps()
    skipStagesAfterUnstable()
  }
  parameters {
    choice(
      name: 'PIPELINE',
      choices: 'TEST\nSTAGE',
      description: 'Which Pipeline to deploy to?'
    )
    string(
      name: 'GI_BRANCH_NAME',
      defaultValue: 'main',
      description: 'Git Branch name to deploy'
    )
    string (
      name : 'RELEASE_VERSION',
      defaultValue: 'latest',
      description: 'Application Docker release tag (e.g. latest, 1.0.0.15). Please use this if you need to deploy to stage (If left blank, new build and deploy to test will be made).'
    )
    string (
      defaultValue: '7355961d-5def-4ed2-b187-32d3fdb3370f'
      description: 'The ASoC app ID to associate this scan with. This should typically be left alone as it will default to the app ID that was configured for your project during onboarding.',
      name: 'appID',
      trim: true
    )
    string (
      defaultValue: '',
      description: 'target dir to scan using ASoC ex: src or dist or blank to scan entire workspace',
      name: 'targetDir'
      trim: true
    )
    booleanParam (
      defaultValue: false,
      description: 'boolean for toggling static scan: [true] - run scan stage [false] - skip scan stage',
      name: 'runStaticScan'
    )
  }
  stages {
    stage('Setup') {
      steps {
        deleteDir()
        // checkout scm
        checkRemedyStatus prod: "false"
        script {
          branchName = params.GIT_BRANCH_NAME.trim()
          echo "env.GIT_BRANCH_NAME ${branchName}"
          commitHash = checkout([$class: 'GitSCM',
                                            branches: [[name: branchName]],
                                            doGenerateSubmoduleConfigurations: false,
                                            extensions: [[$class: 'CleanCheckout']],
                                            submoduleCfg: [],
                                            userRemoteConfigs: [[url:GIT_URL]]
                                    ]).GIT_COMMIT
          wspace.init()

          APP_NAME= build.getNodejsAppName()
          APP_VERSION= build.getNodejsAppVersion()

          if (RELEASE_VERSION == 'latest') {
            APP_RELEASE_TAG='${APP_VERSION}.${BUILD_NUMBER}'
          } else {
            APP_RELEASE_TAG='${RELEASE_VERSION}'
          }
          echo "APP_NAME='${APP_NAME}', APP_VERSION='${APP_VERSION}', RELEASE_VERSION='${RELEASE_VERSION}', APP_RELEASE_TAG='${APP_RELEASE_TAG}''"
          if (currentBuild.description == null) {
            currentBuild.description = ''
          }
          currentBuild.description = "${currentBuild.description} ${PIPELINE}: ${APP_RELEASE_TAG}"
        }
      }
    }

    stage('Build') {
      when {
        allOf {
          not { environment name: 'gitlabActionType', value: 'MERGE' }
          expression { params.PIPELINE == 'TEST' || params.PIPELINE == 'STAGE' }
        }
      }
      steps {
        sh """
          echo 'Build Number: ' ${BUILD_NUMBER} > current.version.txt
          echo 'Git Commit: ' ${GIT_COMMIT} >> current.version.txt
          echo 'Git URL: ' ${GIT_URL} >> current.version.txt
          echo 'App Release Tag: ' ${APP_RELEASE_TAG} >> current.version.txt
          echo 'App Version: ' ${APP_VERSION} >> current.version.txt
          echo 'Created On: '`date` >> current.version.txt
          npm ci
          ls -al node_modules
          npm run build
        """
      }
    }

    stage('Build Image') {
      when {
        allOf {
          not { environment name: 'gitlabActionType', value: 'MERGE' }
          expression { params.PIPELINE == 'TEST' || params.PIPELINE == 'STAGE' }
          expression { params.RELEASE_VERSION == 'latest'}
        }
      }
      steps {
        script {
          sh """
            mkdir tmp
            ls -lsrt
            tar -zc -f ./tmp/${APP_NAME}=${APP_RELEASE_TAG}.gz --exclude-from=.dockerignore --exclude=tmp .
            du -sh *
          """
          ose.oseLoginDev(constantsutil.FLBLUE_OSE4X_APPS)
          ose.buildImage('gpt-image', APP_NAME, APP_RELEASE_TAG, false, "./tmp/${APP_NAME}-${APP_RELEASE_TAG}.gz")
          ose.oseLogout()
        }
      }
    }
    stage('Run ASoC Scan') {
      when {
        expression { params.runStaticScan == 'true' || params.runStaticScan == true }
      }
      steps {
        runHCLAppScan target: "${params.targetDir}", appID: "${params.appID}"
      }
    }
    stage('Deploy: Test') {
      when {
        allOf {
          not { environment name: 'gitlabActionType', value: 'MERGE' }
          expression { params.PIPELINE == 'TEST' }
        }
      }
      steps {
        script {
          ose.oseLoginDev(constantsutil.FLBLUE_OSE4X_APPS)
          ose.deploy('gpt-test', APP_NAME, "test", APP_RELEASE_TAG)
          ose.oseLogout()
        }
      }
    }
    stage('Deploy: stage') {
      when { expression { params.PIPELINE == 'STAGE' } }
      steps {
        script {
          ose.oseLoginDev(constantsutil.FLBLUE_OSE4X_APPS)
          ose.deploy('gpt-stage', APP_NAME, "stage", APP_RELEASE_TAG)
          ose.oseLogout()
        }
      }
    }
    post {
      always {
        script {
          def subject = "JENKINS DEPLOY #${BUILD_NUMBER} | ${APP_NAME}@${APP_RELEASE_TAG}"
          def body = """
                      Jenkins Pipeline #${BUILD_NUMBER} Completed<br>
                      <br>
                      <b>Job:</b> <code>${JOB_NAME}</code><br>
                      <b>App:</b> ${APP_NAME}<br>
                      <b>Branch:</b> ${branchName}<br>
                      <b>Deploy:</b> ${params.PIPELINE}<br>
                      <b>Tag:</b> <code>${APP_RELEASE_TAG}</code><br>
                      <br>
                      BUILD URL: ${BUILD_URL}<br>
                      """.stripIndent()
          emailext(
            subject: subject,
            body: body,
            replyTo: '$DEFAULT_REPLYTO',
            to: '${MAIL_TO}'
          )
        }
        cleanWs()
      }
    }
  }
}

