import { fetchAndUpdateModelDetails, updateModels } from "$lib/server/models.js";
import { error } from "@sveltejs/kit";

export async function GET({ locals, url }) {
	let resGet = JSON.stringify({});
	if (url.searchParams.get("reason") === "use-db-model-details") {
		const activeModel = url.searchParams.get("active-model");
		if (!activeModel) throw error(400, "active-model is required");
		console.log("~#~ triggered GET");
		// get params for new active model
		await fetchAndUpdateModelDetails({ locals, activeModel, from: "get" });
		resGet = JSON.stringify({ message: "success" });
	}
	return new Response(resGet);
}

export async function POST({ request }) {
	const body = await request.json();

	// update temperature to db when slider adjusted
	console.log(`tempr POST: `, body.models.parameters.temperature);
	updateModels(body.models, body.activeModel);

	return new Response(JSON.stringify({ message: "success" }));
}
