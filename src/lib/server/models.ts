import {
	HF_TOKEN,
	HF_API_ROOT,
	MODELS,
	OLD_MODELS,
	TASK_MODEL,
	HF_ACCESS_TOKEN,
} from "$env/static/private";
import type { ChatTemplateInput } from "$lib/types/Template";
import { compileTemplate } from "$lib/utils/template";
import { z } from "zod";
import endpoints, { endpointSchema, type Endpoint } from "./endpoints/endpoints";
import endpointTgi from "./endpoints/tgi/endpointTgi";
import { sum } from "$lib/utils/sum";
import { collections } from "$lib/server/database";
import { authCondition } from "$lib/server/auth";
import type { ModelDetails } from "$lib/types/Model";

const DEFAULT_MODEL =
	'[{ "name": "mistralai/Mistral-7B-Instruct-v0.1", "displayName": "mistralai/Mistral-7B-Instruct-v0.1", "description": "Mistral 7B is a new Apache 2.0 model, released by Mistral AI that outperforms Llama2 13B in benchmarks.", "websiteUrl": "https://mistral.ai/news/announcing-mistral-7b/", "preprompt": "", "chatPromptTemplate" : "", "parameters": {"temperature": 0.1, "top_p": 0.95, "repetition_penalty": 1.2, "top_k": 50, "truncate": 3072, "max_new_tokens": 1024, "stop": ["</s>"]}, "promptExamples": [{"title": "What is Generative AI?", "prompt":"Define Generative AI"}]}]';

type Optional<T, K extends keyof T> = Pick<Partial<T>, K> & Omit<T, K>;

const modelConfig = z.object({
	/** Used as an identifier in DB */
	id: z.string().optional(),
	/** Used to link to the model page, and for inference */
	name: z.string().min(1),
	displayName: z.string().min(1).optional(),
	description: z.string().min(1).optional(),
	websiteUrl: z.string().url().optional(),
	modelUrl: z.string().url().optional(),
	datasetName: z.string().min(1).optional(),
	datasetUrl: z.string().url().optional(),
	userMessageToken: z.string().default(""),
	userMessageEndToken: z.string().default(""),
	assistantMessageToken: z.string().default(""),
	assistantMessageEndToken: z.string().default(""),
	messageEndToken: z.string().default(""),
	preprompt: z.string().default(""),
	prepromptUrl: z.string().url().optional(),
	chatPromptTemplate: z
		.string()
		.default(
			"{{preprompt}}" +
				"{{#each messages}}" +
				"{{#ifUser}}{{@root.userMessageToken}}{{content}}{{@root.userMessageEndToken}}{{/ifUser}}" +
				"{{#ifAssistant}}{{@root.assistantMessageToken}}{{content}}{{@root.assistantMessageEndToken}}{{/ifAssistant}}" +
				"{{/each}}" +
				"{{assistantMessageToken}}"
		),
	promptExamples: z
		.array(
			z.object({
				title: z.string().min(1),
				prompt: z.string().min(1),
			})
		)
		.optional(),
	endpoints: z.array(endpointSchema).optional(),
	parameters: z
		.object({
			temperature: z.number().min(0).max(1),
			truncate: z.number().int().positive().optional(),
			max_new_tokens: z.number().int().positive(),
			stop: z.array(z.string()).optional(),
			top_p: z.number().positive().optional(),
			top_k: z.number().positive().optional(),
			repetition_penalty: z.number().min(-2).max(2).optional(),
		})
		.passthrough()
		.optional(),
	multimodal: z.boolean().default(false),
	unlisted: z.boolean().default(false),
});

const modelFetch = MODELS || process.env.MODELS || DEFAULT_MODEL;
// console.log("modelFetch: ", modelFetch);
const modelsRaw = z.array(modelConfig).parse(JSON.parse(modelFetch));

const processModel = async (m: z.infer<typeof modelConfig>) => ({
	...m,
	userMessageEndToken: m?.userMessageEndToken || m?.messageEndToken,
	assistantMessageEndToken: m?.assistantMessageEndToken || m?.messageEndToken,
	chatPromptRender: compileTemplate<ChatTemplateInput>(m.chatPromptTemplate, m),
	id: m.id || m.name,
	displayName: m.displayName || m.name,
	preprompt: m.prepromptUrl ? await fetch(m.prepromptUrl).then((r) => r.text()) : m.preprompt,
	parameters: {
		...m.parameters,
		stop_sequences: m.parameters?.stop,
		// temperature: 0.85,
	},
});

const addEndpoint = (m: Awaited<ReturnType<typeof processModel>>) => ({
	...m,
	getEndpoint: async (): Promise<Endpoint> => {
		if (!m.endpoints) {
			return endpointTgi({
				type: "tgi",
				url: `${HF_API_ROOT}/${m.name}`,
				accessToken: HF_TOKEN ?? HF_ACCESS_TOKEN,
				weight: 1,
				model: m,
			});
		}
		const totalWeight = sum(m.endpoints.map((e) => e.weight));

		let random = Math.random() * totalWeight;

		for (const endpoint of m.endpoints) {
			if (random < endpoint.weight) {
				const args = { ...endpoint, model: m };

				switch (args.type) {
					case "tgi":
						return endpoints.tgi(args);
					case "aws":
						return await endpoints.aws(args);
					case "openai":
						return await endpoints.openai(args);
					case "llamacpp":
						return endpoints.llamacpp(args);
					case "ollama":
						return endpoints.ollama(args);
					default:
						// for legacy reason
						return endpoints.tgi(args);
				}
			}
			random -= endpoint.weight;
		}

		throw new Error(`Failed to select endpoint`);
	},
});

// const user = async () => await collections.users.findOne({});

export const processedModels = await Promise.all(
	modelsRaw.map((e) => processModel(e).then(addEndpoint))
);

function updatedModelData(modelDetails: ModelDetails): typeof processedModels {
	// console.log(`modelDetails: `, modelDetails);
	return processedModels.map((currModel) => {
		// directly changing temperature as there's only one model
		// TODO: find right model before changing its temperature
		//   when changing this also change startingTemperature
		//   to get not first model but right model's temperature(?)

		let updatedData = { ...currModel };
		if (currModel.name === modelDetails.name) {
			// console.log(`modelDetails: `, modelDetails);
			updatedData = {
				...updatedData,
				parameters: {
					...currModel.parameters,
					temperature: modelDetails.parameters.temperature,
				},
			};
		}

		// console.log(`updatedData: `, updatedData);
		return updatedData;
	});
}

// NOTE: load / change parameters here
// NOTE: this is triggered whenever slider changes
// replace processedModels[0] with defaultModel
const startingTemperature = processedModels[0].parameters.temperature || 0.1;
let models = updatedModelData({
	name: processedModels[0].name,
	parameters: { temperature: startingTemperature },
});

// TODO: send if (newData) models = newData to updatedModelData
//   to merge updatedModels and updatedModelData into one
export function updateModels(modelDetails: ModelDetails, activeModel: string) {
	// console.log(`modelDetails?.parameters?.temperature: `, modelDetails?.parameters?.temperature);
	// console.log(`activeModel: `, activeModel);
	if (!modelDetails?.name || !modelDetails?.parameters?.temperature || !activeModel) {
		console.error(`updateModels: invalid modelDetails: `, modelDetails);
	}
	console.log(`tempr received um: `, modelDetails.parameters.temperature);
	const newData = updatedModelData(modelDetails);
	if (newData.length) {
		models = [...newData];
	}
	const matchedModel = models.find((m) => m.name === activeModel);
	// console.log(`activeModel: `, activeModel);
	console.log(`tempr models after update: `, matchedModel?.parameters?.temperature);
}

export async function fetchAndUpdateModelDetails({
	locals,
	activeModel,
	from,
}: {
	locals: App.Locals;
	activeModel: string;
	from: string;
}) {
	console.log(`## faumd called from ${from}`);
	// update on app loads
	// gets temperature from db's conversation and updates app temperature

	// const lastConversation = await collections.conversations
	// .find(authCondition(locals), { sort: { updatedAt: -1 }, limit: 1 })
	// .toArray();
	// const messages = lastConversation?.[0]?.messages || [];
	// const lastMessage = messages[messages.length - 1];

	// TODO:
	//   get active model name
	//   go through conversations in reverse order of updated date
	//   and find conversation with temperature rel to active model name
	//   send it to updateModels()
	//   this is to set temperature of all models to latest user set temperature
	//   may be there should be better way to set temperature

	// gets latest active model details
	async function getModelDetails(activeModel: string): Promise<ModelDetails> {
		async function fetchLatestConvs() {
			const latestConvs = await collections.conversations
				.find(authCondition(locals), { sort: { updatedAt: -1 }, limit: 10 })
				.toArray();
			// console.log(`latestConvs: `, JSON.stringify(latestConvs, null, 2));
			let latestActiveConvsMsg = {};
			latestConvs.find((cMsgs) => {
				const latestMsgs = cMsgs?.messages.reverse();
				const latestActiveMsg = latestMsgs.find((cMsg) => cMsg?.modelDetail.name === activeModel);
				// console.log(`latestActiveMsg: `, latestActiveMsg);
				latestActiveConvsMsg = latestActiveMsg;
				return latestActiveMsg;
			});
			return latestActiveConvsMsg;
		}
		const latestActiveConvs = await fetchLatestConvs();
		// console.log(`latestActiveConvs: `, latestActiveConvs);
		// TODO: if !latestActiveConvs, paginate through till you get it
		// const latestUserActiveConvs = latestActiveConvs?.messages.find((msg) => msg.from === "user");
		const latestUserActiveConvs = latestActiveConvs;
		// console.log(`latestUserActiveConvs: `, latestUserActiveConvs);
		return latestUserActiveConvs?.modelDetail;
	}

	// const settings = await collections.settings.findOne(authCondition(locals));
	const tempActiveModelDict = [
		"mistralai/Mistral-7B-Instruct-v0.1",
		"mistralai/Mixtral-8x7B-Instruct-v0.1",
		"codellama/CodeLlama-34b-Instruct-hf",
	];
	console.log(`activeModel model: `, tempActiveModelDict.indexOf(activeModel) + 1);
	const resModelDetails = await getModelDetails(activeModel);

	console.log(
		`tempr faumd: `,
		resModelDetails?.name + " - " + resModelDetails?.parameters?.temperature
	);
	if (!resModelDetails?.parameters?.temperature) return;
	const modelDetails: ModelDetails = {
		name: resModelDetails?.name,
		parameters: {
			temperature: resModelDetails.parameters.temperature,
		},
	};
	// temperature:
	//  user changes temperature
	//    → +[...model].server → updateModels() - doesn't persist
	//    → +[id].server → sends conversation data w/ temperature as modelDetails - persists via db
	//  when app is loaded
	//    → +layout.server → gets latest conversation for active model and sets its temperature
	updateModels(modelDetails, activeModel);
}

export const defaultModel = models[0];

// Models that have been deprecated
export const oldModels = OLD_MODELS
	? z
			.array(
				z.object({
					id: z.string().optional(),
					name: z.string().min(1),
					displayName: z.string().min(1).optional(),
				})
			)
			.parse(JSON.parse(OLD_MODELS))
			.map((m) => ({ ...m, id: m.id || m.name, displayName: m.displayName || m.name }))
	: [];

export const validateModel = (_models: BackendModel[]) => {
	// Zod enum function requires 2 parameters
	return z.enum([_models[0].id, ..._models.slice(1).map((m) => m.id)]);
};

// if `TASK_MODEL` is the name of a model we use it, else we try to parse `TASK_MODEL` as a model config itself

export const smallModel = TASK_MODEL
	? (models.find((m) => m.name === TASK_MODEL) ||
			(await processModel(modelConfig.parse(JSON.parse(TASK_MODEL))).then((m) =>
				addEndpoint(m)
			))) ??
	  defaultModel
	: defaultModel;

export { models };

export type BackendModel = Optional<
	typeof defaultModel,
	"preprompt" | "parameters" | "multimodal" | "unlisted"
>;
