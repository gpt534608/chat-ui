FROM quay-enterprise.apps.ose4-dev.bcbsfl.com/ocp-image/nodejs-18-minnimal-ubi8:latest
WORKDIR /opt/app-root
COPY --chown=1001:1001 . .
USER 1001
ENTRYPOINT ["node", "build/index.js"]
